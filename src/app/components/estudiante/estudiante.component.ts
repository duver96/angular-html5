import { Component, OnInit } from '@angular/core';
import { Estudiante} from '../../clases/estudiante';

@Component({
  selector: 'app-estudiante',
  templateUrl: './estudiante.component.html',
  styleUrls: ['./estudiante.component.css']
})
export class EstudianteComponent implements OnInit {


  constructor() { }

  ngOnInit(): void {
  }
  
estudiantes: Estudiante[] = [
    { codigo: 1, nombre: 'Duver', apellido: 'Carmona', nota: 5 },
    { codigo: 2, nombre: 'Lizeth', apellido: 'Montoya', nota: 4.8 },
    { codigo: 3, nombre: 'Rosa', apellido: 'Ochoa', nota: 3.0 },
    { codigo: 4, nombre: 'Jaider', apellido: 'Carmona', nota: 2.8 },
    { codigo: 5, nombre: 'Cristian', apellido: 'Marin', nota: 3.0 },
    { codigo: 6, nombre: 'Andres', apellido: 'Poseto', nota: 1.5 },
    { codigo: 7, nombre: 'Juan D', apellido: 'Carmona', nota: 5 },
    { codigo: 8, nombre: 'Paulina', apellido: 'Carmona', nota: 3.6 },
    { codigo: 9, nombre: 'Sandra', apellido: 'Garcia', nota: 4.0 },
    { codigo: 10, nombre: 'Zeus', apellido: 'Gato', nota: 1.0 },
  ];

  validarSiGana(nota: number): boolean {
    if (nota < 3) {
      return false
    }
    else if (nota >= 3.0) {
      return true;
    }
  }
   
}
