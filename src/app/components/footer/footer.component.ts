import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
})
export class FooterComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    var fecha = new Date(),
        horas = fecha.getHours(),
        ampm,
        diaSemana = fecha.getDay(),
        min = fecha.getMinutes().toString(),
        seg = fecha.getSeconds().toString(),
        dia = fecha.getDate(),
        mes = fecha.getMonth(),
        year = fecha.getFullYear();

      let pHoras = document.getElementById('horas'),
        pAmPm = document.getElementById('ampm'),
        PDiaSemana = document.getElementById('diaSemana'),
        pMin = document.getElementById('min'),
        pSeg = document.getElementById('seg'),
        pDia = document.getElementById('dia'),
        pMes = document.getElementById('mes'),
        pYear = document.getElementById('year');

      var semana = [
        'Domingo',
        'Lunes',
        'Martes',
        'Miercoles',
        'Jueves',
        'Viernes',
        'Sánbado',
      ];
      PDiaSemana.textContent = semana[diaSemana];
      pDia.textContent = dia.toString();
      var meses = [
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre',
      ];
      pMes.textContent = meses[mes];
      pYear.textContent = year.toString();

      if (horas >= 12) {
        horas = horas - 12;
        ampm = 'PM';
      } else {
        ampm = 'AM';
      }
      if (horas == 0) {
        horas = 12;
      }
      pHoras.textContent = horas.toString();
      pAmPm.textContent = ampm;
      
      if (parseInt(min) < 10) {
        min = "0" +min;
      }
      if (parseInt(seg) < 10) {
        seg = "0" + seg;
      }
      pMin.textContent = min.toString();
      pSeg.textContent = seg.toString();
      var repeticiones = setInterval(this.ngOnInit, 1000);
  }

  
   

  
}
